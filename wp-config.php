<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'consejo');

/** MySQL database username */
define('DB_USER', 'consejo');

/** MySQL database password */
define('DB_PASSWORD', 'c0ns3j0');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '](4*{_gO#E79j|5FG$G^EtUK!WtMmVRB$DDUWh~:7!2L`mfhR;#-B0|RLbe!4Cc4');
define('SECURE_AUTH_KEY',  'QA4>[U$$oQ5z%1!N6 3}+2)9=nnib`1dqc_iA8]v.{z& d`5{BEm?JaO|T)07VJ~');
define('LOGGED_IN_KEY',    'W*W:S}N!F[$HwJgE{0_b|a))r>a;Ix|m(K2Y0P/Z.X)MlfEPY1nQe*#1v9A }1K~');
define('NONCE_KEY',        'Vf{^O/Tb(r$L(HMfp{8sB7&%1k:2.k0 C%{v,5i!qU@L3gl;_SWA{@[h 1+ljgG[');
define('AUTH_SALT',        'D@;2VRlxbhi^D60`/|9HjL$iq+q+)p*.dV(Q $njj[ZrAu{-zp|dn;RIX|C:P N+');
define('SECURE_AUTH_SALT', '3(RL|ova(FE)>uwwM0y+rU9@L=)x$;sycrW<QE,=#e6g+7wY,YYVs!%|+rw_GFhc');
define('LOGGED_IN_SALT',   ':=HA|>!ZzOYGRSmqJMPG|6|X!U~NrqR~?9|vAOG|y%Ic7{L&2{2+yPh0VYFNIj>(');
define('NONCE_SALT',       ')T3/f)oYcx0S!6Z(y?fap9FWv)@<VaOhpk>g/^rb:unRCY;A8+3`TPPRkq8E!8o ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'zn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
